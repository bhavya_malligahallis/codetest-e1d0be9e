
function addTask(newData) {
    $(".edit-box").slideUp();
    $("#task-title, #task-id").val("");
    $.ajax({
        url: "/tasks",
        method: "POST",
        data: {property: newData},
        success: function (response) {
            if(response.STATUS == "SUCCESS") {
                window.location.reload(true);
            }
            else {
                $(".message").show().text("Unable to add task");
            }
        },
        error: function (response) {
            if(response.STATUS == "SUCCESS") {
                window.location.reload(true);
            }
            else {
                $(".message").show().text("Unable to add task");
            }
        }
    });
}
function updateTask(id, newData) {
    $(".edit-box").slideUp();
    $("#task-title, #task-id").val("");
    $.ajax({
        url: "/tasks",
        method: "PUT",
        data: {id: id, property: newData},
        success: function (response) {
            if(response.STATUS == "SUCCESS") {
                window.location.reload(true);
            }
            else {
                $(".message").show().text("Unable to update task");
            }
        },
        error: function (response) {
            $(".message").show().text("Error in update task");
        }
    });
}
function openAddTask() {
    $(".message").hide();
    $("#task-title, #task-id").val("");
    $(".edit-box").slideDown();
}
function openEditTask(id) {
    $(".message").hide();
    $.ajax({
        url: "/tasks?id=" + id,
        method: "get",
        success: function (response) {
            if(response.STATUS == "SUCCESS") {
                $("#task-title").val(response.entity.task);
                $("#task-id").val(response.entity.id);
                $(".edit-box").slideDown();
            }
            else {
                $(".message").show().text("Unable to Find Record, please refresh!");
            }
        },
        error: function (response) {

        }
    });
}
function saveTask() {
    $(".message").hide();
    if($.trim($("#task-title").val()) == "" ) {
        $(".message").show().text('Please enter Task Title');
    }
    else {
        if( $("#task-id").val() == "") {
            addTask({title: $("#task-title").val()});
        }
        else {
            updateTask($("#task-id").val(), {title: $("#task-title").val()});
        }
    }
}
function deleteTask(id) {
    $(".message").hide();
    $(".edit-box").slideUp();
    $("#task-title, #task-id").val("");
    $.ajax({
        url: "/tasks",
        method: "DELETE",
        data: {id: id},
        success: function (response) {
            if(response.STATUS == "SUCCESS") {
                window.location.reload(true);
            }
            else {
                $(".message").show().text("Unable to DELETE task");
            }
        },
        error: function (response) {
            $(".message").show().text("Error in delete task");
        }
    });
}
function sortTasks() {
    $(".message").hide();
    var newOrder = [];
    $(".tasks").each(function(i) {
        newOrder.push(parseInt($(this).attr("id").replace("task_","")));
    });
    $(".edit-box").slideUp();
    $("#task-title, #task-id").val("");
    $.ajax({
        url: "/tasks",
        method: "PUT",
        data: {order: newOrder.toString()},
        success: function (response) {
            if(response.STATUS == "SUCCESS") {
                //window.location.reload(true);
            }
            else {
                $(".message").show().text("Unable to update order");
            }
        }
    });
}
function markComplete(id) {
    $(".message").hide();
    $.ajax({
        url: "/tasks",
        method: "PUT",
        data: {id: id, property: {status: "Complete"}},
        success: function (response) {
            if(response.STATUS == "SUCCESS") {
                window.location.reload(true);
            }
            else {
                $(".message").show().text("Unable to update task");
            }
        },
        error: function (response) {
            $(".message").show().text("Error in update task");
        }
    });
}
/*Page Ready*/
$(document).ready(function(){
    $("#list-task").sortable();
});