var express = require('express'),
	exphbs  = require('express-handlebars'),
	fs = require('fs'),
	url = require('url'),
	app = express();

//app.locals.Datacontent = require('./jsonData.json');
//console.log(app.locals.Datacontent);
app.locals.tasksList = require('./taskList.json');
app.engine('handlebars', exphbs({defaultLayout: 'main'}));
app.set('view engine', 'handlebars');
//For accessing css and js files compiled after gulp has run
app.use('/assets', express.static('public'));
app.use(require("body-parser").urlencoded({
	extended: true
}));
app.use(require("body-parser").json());
app.get('/', function(req, res) {
		res.render('index', {
		name: 'Bhavya'
	});
});
/*For Tasks Requests - these will work as rest APIs - GET*/
app.get('/tasks', function(req, res) {
	console.log(req.url);
	/*Setting Response Headers*/
	res.header("Content-Type", "application/json");
    res.header("Transfer-Encoding", "utf-8");
	var url_parts = url.parse(req.url, true);
	if(!url_parts.query.id) {
		res.json({"STATUS": "ERROR", "message": "Invalid request"});
	}
	else {
		for(var i=0;i<app.locals.tasksList.tasks.length;i++) {
			var elemnt = app.locals.tasksList.tasks[i];
			if(elemnt.id == url_parts.query.id) {
				res.json({"STATUS": "SUCCESS", "entity": elemnt});
				break;
			}
			else if(i == app.locals.tasksList.tasks.length-1) {
				res.json({"STATUS": "FAILURE", "message": "No task with id `"+url_parts.query.id+"`"});
			}
		}
	}
    res.end();
});
/*For Tasks Requests - these will work as rest APIs - POST*/
app.post('/tasks', function (req, res) {
	if(req.body) {
		var params = req.body;
		var lastId = 0;
		for(var i=0;i<app.locals.tasksList.tasks.length;i++) {
			var elemnt = app.locals.tasksList.tasks[i];
			if(elemnt.id >= lastId) {
				lastId = elemnt.id + 1;
			}
		}
		var newJson = app.locals.tasksList.tasks;
		newJson.push({id: lastId, task: req.body.property.title, status: "Incomplete"});
		writeJSON(JSON.stringify({tasks: newJson}));
		res.json({"STATUS": "SUCCESS", "message": "Task added with id `"+lastId+"`"});
	}
	else {
		res.json({"STATUS": "ERROR", "message": "Unable to save Task"});
	}
	res.end();
});
/*For Tasks Requests - these will work as rest APIs - PUT*/
app.put('/tasks', function (req, res) {
	if(req.body) {
		var params = req.body;
		var lastId = -1;
		for(var i=0;i<app.locals.tasksList.tasks.length;i++) {
			var elemnt = app.locals.tasksList.tasks[i];
			if(elemnt.id == params.id) {
				lastId = i;
				break
			}
		}
		var newJson = app.locals.tasksList.tasks;
		if(lastId == -1) {
			if (req.body.order){
				var orderJson = new Array(newJson.length);
				var newOrder = req.body.order.split(",");
				for(var i=0;i<orderJson.length;i++) {
					var currentIndx = parseInt(newOrder[i]);
					orderJson[i] = {
						id: i,
						task: newJson[currentIndx].task,
						status: newJson[currentIndx].status
					};
				}
				app.locals.tasksList.tasks = orderJson;
				writeJSON(JSON.stringify({tasks: orderJson}));
				res.json({"STATUS": "SUCCESS", "message": "Task updated by new order"});
			}
			else {
				res.json({"STATUS": "ERROR", "message": "Unable to save Task"});
			}
		}
		else {
			if(req.body.property.title) {
				newJson[lastId].task = req.body.property.title;
			}
			else if (req.body.property.status) {
				newJson[lastId].status = req.body.property.status;
			}
			writeJSON(JSON.stringify({tasks: newJson}));
			res.json({"STATUS": "SUCCESS", "message": "Task updated with id `"+lastId+"`"});
		}
	}
	else {
		res.json({"STATUS": "ERROR", "message": "Unable to save Task"});
	}
	res.end();
});
/*For Tasks Requests - these will work as rest APIs - DELETE*/
app.delete('/tasks', function (req, res) {
	if(req.body) {
		var params = req.body;
		var lastId = -1;
		for(var i=0;i<app.locals.tasksList.tasks.length;i++) {
			var elemnt = app.locals.tasksList.tasks[i];
			if(elemnt.id == params.id) {
				lastId = i;
				break
			}
		}
		if(lastId == -1) {
			res.json({"STATUS": "ERROR", "message": "Unable to save Task"});
		}
		else {
			var newJson = app.locals.tasksList.tasks;
			newJson.splice(lastId,1);
			writeJSON(JSON.stringify({tasks: newJson}));
			res.json({"STATUS": "SUCCESS", "message": "Task deleted with id `"+lastId+"`"});
		}
	}
	else {
		res.json({"STATUS": "ERROR", "message": "Unable to delete Task"});
	}
	res.end();
});

function writeJSON(newJSON) {
	fs.writeFile('taskList.json', newJSON, function(err) {
		if (err) return console.log(err);
		console.log('Task File Updated');
	});
}
/*Starting Server*/
app.listen(5000);
console.log('Server is running');