// We have to require our dependencies
var gulp = require('gulp');
var $ = require('gulp-load-plugins')();
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
// create a TASK to compile Sass into CSS using gulp-sass
gulp.task('css', function() {
   gulp.src(['./app/scss/*.scss'])
   .pipe($.sass({style: 'expanded'}))
   .pipe(concat('all.css'))
   .pipe(gulp.dest('./public/css'));
});
//Minify and concatenate JS files to one
gulp.task('scripts', function() {
  gulp.src(['./app/js/*.js'])
    .pipe(concat('vendor.js'))
    .pipe(uglify())
    .pipe(gulp.dest('./public/js/'))
});
// create a TASK to WATCH for changes in your files
// this will "watch" for any changes in your files and rerun gulp if necessary
gulp.task('watch', function() {
   gulp.watch(['./app/**/*.js'], ['scripts']);
   gulp.watch(['./app/**/*.scss'], ['css']);
});
// finally, create a TASK that will run all commands when typing "gulp"
// in Terminal

gulp.task('default', ['scripts', 'css', 'watch'], function() {});